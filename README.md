php_quine
=========


Ce programme php affiche son propre code source. Plus d'info ici : https://fr.wikipedia.org/wiki/Quine_(informatique)

Pour tester :
-------------

```
git clone git@gitlab.com:cgrand221/php_quine.git
cd php_quine
php quine.php
```
